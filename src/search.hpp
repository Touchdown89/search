#include <iostream>
#include <string>

using namespace std;

int search(string x, string y)
{
	auto n = 0u;
	
	for(auto i=0u; i < x.size(); i++)
	{
		if(x[i] == y[n])
		{
			auto m = i+1;
	
			while(n < y.size())
			{
				if(x[m] == y[n])
				{
					m++;
					n++;
				}
				else return -1;
								
			}
			return i;
		}
		else return -1;
	}
}
